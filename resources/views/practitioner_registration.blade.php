<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Practitioner Registration</title>
</head>
<body>
    <h1>Practitioner Registration</h1>
    <form action="{{ route('registerpost') }}" method="POST">
        @csrf
        <label for="first_name">First Name:</label><br>
        <input type="text" id="first_name" name="first_name" value="{{ old('first_name') }}"><br>
        @error('first_name')
            <span>{{ $message }}</span><br>
        @enderror

        <label for="last_name">Last Name:</label><br>
        <input type="text" id="last_name" name="last_name" value="{{ old('last_name') }}"><br>
        @error('last_name')
            <span>{{ $message }}</span><br>
        @enderror

        <label for="email">Email:</label><br>
        <input type="email" id="email" name="email" value="{{ old('email') }}"><br>
        @error('email')
            <span>{{ $message }}</span><br>
        @enderror

        <label for="password">Password:</label><br>
        <input type="password" id="password" name="password"><br>
        @error('password')
            <span>{{ $message }}</span><br>
        @enderror

        <label for="password_confirmation">Confirm Password:</label><br>
        <input type="password" id="password_confirmation" name="password_confirmation"><br>

        {{-- Uncomment the lines below if you want to include terms agreement --}}
        {{-- <label for="agree_terms">
            <input type="checkbox" id="agree_terms" name="agree_terms">
            I agree to the <a href="#" class="text-primary">Terms</a>
        </label><br>
        @error('agree_terms')
            <span>{{ $message }}</span><br>
        @enderror --}}

        <button type="submit">Register</button>
    </form>
</body>
</html>
