<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Practitioner;
use App\Models\Hotel;
use Illuminate\Validation\ValidationException;
use Exception;

class AuthController extends Controller
{

    public function loadRegister()
    {
        // if (Auth::user()) {
        //     $route = $this->redirectDash();
        //     return redirect($route);
        // }
        return view('register');
    }

    public function register(Request $request)
    {
        $request->validate([
            'first_name' => 'required|min:2',
            'last_name' => 'nullable|min:2',
            'email' => 'required|email|max:100|unique:users',
            'password' => 'required|confirmed|min:6',
            'agree_terms' => 'required|accepted',
            'user_type' => 'required|in:practitioner,hotel',
        ], [
            'first_name.required' => 'Please enter your first name.',
            'first_name.min' => 'First name must be at least 2 characters.',
            'email.required' => 'Please enter your email address.',
            'email.email' => 'Please enter a valid email address.',
            'email.max' => 'Email address must not exceed 100 characters.',
            'email.unique' => 'This email address is already taken.',
            'password.required' => 'Please enter a password.',
            'password.confirmed' => 'The passwords do not match.',
            'password.min' => 'Password must be at least 6 characters.',
            'agree_terms.required' => 'You must agree to the terms and conditions.',
            'user_type.required' => 'Please select user type.',
            'user_type.in' => 'Invalid user type selected.',
        ]);

        try {
            $user = new User;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            // $user->roles = ($request->user_type === 'practitioner') ? 0 : 1;
            $user->save();

            // // Create associated practitioner or hotel record based on user type
            // if ($request->user_type === 'practitioner') {
            //     $practitioner = new Practitioner;
            //     $practitioner->user_id = $user->id;
            //     $practitioner->nationality = $request->nationality;
            //     $practitioner->country = $request->country;
            //     $practitioner->therapy_choices = $request->therapy_choices;
            //     $practitioner->other_therapies = $request->other_therapies;
            //     $practitioner->qualifications_each_therapies = $request->qualifications_each_therapies;
            //     $practitioner->more_about_you = $request->more_about_you;
            //     $practitioner->work_experience = $request->work_experience;
            //     $practitioner->practitioner_like = $request->practitioner_like;
            //     $practitioner->practitioner_like_choices = $request->practitioner_like_choices;
            //     $practitioner->country_not_like_work = $request->country_not_like_work;
            //     $practitioner->contract_length = $request->contract_length;
            //     $practitioner->availability_start_date = $request->availability_start_date;
            //     $practitioner->describe_yourself = $request->describe_yourself;
            //     $practitioner->languages_spoken = $request->languages_spoken;
            //     $practitioner->link_website = $request->link_website;
            //     $practitioner->heard_about_us = $request->heard_about_us;
            //     $practitioner->save();
            // } else {
            //     $hotel = new Hotel;
            //     $hotel->user_id = $user->id;
            //     $hotel->job_title = $request->job_title;
            //     $hotel->company_name = $request->company_name;
            //     $hotel->save();
            // }


            return redirect('/login')->with('success', 'Your Registration has been successful.');
        } catch (ValidationException $e) {
            // If validation fails, redirect back with errors
            return redirect()->back()->withErrors($e->errors())->withInput();
        } catch (Exception $e) {
            // Handle other exceptions (e.g., database errors) here

            // You can log the exception or display an error message
            return redirect()->back()->with('error', 'An error occurred during registration. Please try again.');
        }
    }


    public function loadLogin()
    {
        // if (Auth::user()) {
        //     $route = $this->redirectDash();
        //     return redirect($route);
        // }
        return view('login');
    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'email.required' => 'Please enter your email address.',
            'email.email' => 'Please enter a valid email address.',
            'password.required' => 'Please enter your password.',
        ]);
        $user = User::where('email', $request->email)->first();
    
        if (!$user) {
            return back()->withErrors([
                'email' => ' Email is not registered.',
            ]);
        }
        // Check if the user is active
        if ($user->status == 0) {
            return back()->withErrors([
                'email' => 'Your account is not active.',
            ]);
        }
    
        // If user exists, try to authenticate
        $userCredential = $request->only('email', 'password');
        if (Auth::attempt($userCredential, $request->has('remember'))) {
            return redirect($this->redirectDash());
        } else {
            return back()->withErrors([
                'password' => ' Password is incorrect.',
            ]);
        }
    }
    
    public function redirectDash()
    {
        $redirect = '';
    
        if (Auth::user() && Auth::user()->roles == 1) {
            $redirect = '/admin/dashboard';
        } else if (Auth::user() && Auth::user()->roles == 2) {
            $redirect = '/hotel/dashboard';
        } else if (Auth::user() && Auth::user()->roles == 0) {
            $redirect = '/practitioners/dashboard';
        } else {
            // Handle the case if the user role is not defined
        }
    
        return $redirect;
    }
    

}
