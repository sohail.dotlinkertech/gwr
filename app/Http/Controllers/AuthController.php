<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Practitioner;
use App\Models\Hotel;
use Illuminate\Validation\ValidationException;
use Exception;

class AuthController extends Controller
{

    public function loadRegister()
    {
        return view('practitioner_registration');
    }

    public function register(Request $request)
    {
        $request->validate([
            'first_name' => 'required|min:2',
            'last_name' => 'nullable|min:2',
            'email' => 'required|email|max:100|unique:users',
            'password' => 'required|confirmed|min:6',
            // Uncomment the line below if you want to enforce terms agreement
            // 'agree_terms' => 'required|accepted',
        ], [
            'first_name.required' => 'Please enter your first name.',
            'first_name.min' => 'First name must be at least 2 characters.',
            'email.required' => 'Please enter your email address.',
            'email.email' => 'Please enter a valid email address.',
            'email.max' => 'Email address must not exceed 100 characters.',
            'email.unique' => 'This email address is already taken.',
            'password.required' => 'Please enter a password.',
            'password.confirmed' => 'The passwords do not match.',
            'password.min' => 'Password must be at least 6 characters.',
            // Uncomment the lines below if you want to enforce terms agreement
            // 'agree_terms.required' => 'You must agree to the terms and conditions.',
            // 'agree_terms.accepted' => 'You must agree to the terms and conditions.',
        ]);

        try {
            $user = new User;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            return redirect('/login')->with('success', 'Your Registration has been successful.');
        } catch (ValidationException $e) {
            return redirect()->back()->withErrors($e->errors())->withInput();
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'An error occurred during registration. Please try again.');
        }
    }


    public function loadLogin()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'email.required' => 'Please enter your email address.',
            'email.email' => 'Please enter a valid email address.',
            'password.required' => 'Please enter your password.',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return redirect('/dashboard'); 
        } else {
            return back()->withErrors([
                'password' => 'Invalid email or password.',
            ]);
        }
    }
    

}
