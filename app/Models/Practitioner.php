<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Practitioner extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'nationality', 'country', 'therapy_choices', 'other_therapies',
        'qualifications_each_therapies', 'more_about_you', 'work_experience',
        'practitioner_like', 'practitioner_like_choices', 'country_not_like_work',
        'contract_length', 'availability_start_date', 'describe_yourself',
        'languages_spoken', 'link_website', 'heard_about_us'
    ];

    // You might want to define a relationship with the User model if necessary
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
