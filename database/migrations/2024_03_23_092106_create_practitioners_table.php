<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePractitionersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practitioners', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');

            $table->string('nationality')->nullable();
            $table->string('country')->nullable();
            $table->text('therapy_choices')->nullable();
            $table->string('other_therapies')->nullable();
            $table->string('qualifications_each_therapies')->nullable();
            $table->text('more_about_you')->nullable();
            $table->text('work_experience')->nullable();
            $table->string('practitioner_like')->nullable();
            $table->string('practitioner_like_choices')->nullable();
            $table->string('country_not_like_work')->nullable();
            $table->string('contract_length')->nullable();
            $table->date('availability_start_date')->nullable();
            $table->text('describe_yourself')->nullable();
            $table->string('languages_spoken')->nullable();
            $table->string('link_website')->nullable();
            $table->string('heard_about_us')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practitioners');
    }
}
